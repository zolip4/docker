# docker




# для установки дополнительных экстеншенов  добавляем в  свой DockerFile после установки основных экстеншенов
    ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
    RUN chmod uga+x /usr/local/bin/install-php-extensions && sync
    RUN install-php-extensions tidy


# начало установки

    sudo nano /etc/NetworkManager/conf.d/00-use-dnsmasq.conf
    
#Содержимое которого:


    #This enabled the dnsmasq plugin.
    [main]
    dns=dnsmasq
        
#Выполнить команду для добавления локального TLD:


    sudo echo 'address=/.docker.loc/127.0.0.1' | sudo tee /etc/NetworkManager/dnsmasq.d/00-docker.loc-tld.conf
#Сохраняем работоспособность файла /etc/hosts:


    sudo echo 'addn-hosts=/etc/hosts' | sudo tee /etc/NetworkManager/dnsmasq.d/01-hosts.conf
#Выполнить команду для обеспечения совместимости:


    sudo rm /etc/resolv.conf && sudo systemctl disable --now systemd-resolved && sudo systemctl reload NetworkManager
#Проверить правильность настройки командой:


    host test.docker.loc
#В результате должен быть примерно такой ответ:


    test.docker.loc has address 127.0.0.1



#Xdebugcli – скрипт который ранит в нужной мадженто xdebug для cli команд bin/magento

    cd data/7.2/magentoexample
    xdebugcli mod:stat

#Скрипт elastic запускает нужный вам (по выбору elasticsearch)

    ./elastic elasticsearch/docker-compose-elas5.6.yml(6.8, 7.4)


#Magcompose – скрипт который запускает команды composer внутри контейнеров.  

    cd data/7.2/magentoexample
    magcompose instal

#Varnishcc - скрипт который чистит кэш  варниша.

    varnishcc example-varnish.docker.loc (подставляете свой урл мадженто)

#Xdebugsh -скрипт для включения и выключения xdebug в контейнерах.

    xdebugsh enable php73(или контейнер который вам нужен)
    Для выключения 
    xdebugsh disable php73 (или контейнер который вам нужен)

#подрубаем эластик

    https://i.imgur.com/b9T279T.png
