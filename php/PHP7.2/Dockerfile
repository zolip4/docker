FROM php:7.2-fpm-buster

# Install dependencies
ENV COMPOSER_ALLOW_SUPERUSER 1
RUN apt-get update \
  && apt-get install -y \
    procps \
    libfreetype6-dev \
    libldap2-dev \
    libicu-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libpng-dev \
    unzip \
    libsodium-dev \
    libxslt1-dev \
    webp \
    optipng \
    jpegoptim \
    jq \
    gifsicle


# Configure the gd library
RUN docker-php-ext-configure \
  gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/
# Install required PHP extensions
RUN docker-php-ext-install \
  dom \
  gd \
  intl \
  mbstring \
  pdo_mysql \
  xsl \
  zip \
  soap \
  ldap \
  bcmath \
  xmlwriter \
  simplexml \
  opcache \
  mysqli \
  sockets

RUN apt-get install -y git

#Install redis extension
RUN pecl install -o -f redis \
&&  rm -rf /tmp/pear \
&&  echo "extension=redis.so" > /usr/local/etc/php/conf.d/redis.ini

#Install mysql-client
RUN DEBIAN_FRONTEND=noninteractive apt-get update -qq \ 
  && DEBIAN_FRONTEND=noninteractive apt-get install -qq -y --no-install-recommends --no-install-suggests \
  default-mysql-client

#Install xdebug and configure
RUN yes | pecl install xdebug-2.9.8

RUN echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_connect_back=1" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.profiler_enable = 0" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.profiler_enable_trigger = 1" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.profiler_output_dir = "/home/7.2"" >> /usr/local/etc/php/conf.d/xdebug.ini

#Disable xdebug on start

RUN sed -i -e 's/^zend_extension/\;zend_extension/g' /usr/local/etc/php/conf.d/xdebug.ini

#Configure sendmail and php

RUN echo "memory_limit=2048M" > /usr/local/etc/php/conf.d/magento.ini \
    && echo "realpath_cache_size=2M" >> /usr/local/etc/php/conf.d/magento.ini \
    && echo "opcache.max_accelerated_files=200000" >> /usr/local/etc/php/conf.d/magento.ini \
    && echo "opcache.memory_consumption=256M" >> /usr/local/etc/php/conf.d/magento.ini \
    && echo "zlib.output_compression=1" >> /usr/local/etc/php/conf.d/magento.ini \
    && echo "max_execution_time = 600" >> /usr/local/etc/php/conf.d/magento.ini \
    && sed -i 's/pm.max_children = 5/pm.max_children = 50/g' /usr/local/etc/php-fpm.d/www.conf \
    && sed -i 's/pm.start_servers = 2/pm.start_servers = 5/g' /usr/local/etc/php-fpm.d/www.conf \
    && sed -i 's/pm.min_spare_servers = 1/pm.min_spare_servers = 5/g' /usr/local/etc/php-fpm.d/www.conf \
    && sed -i 's/pm.max_spare_servers = 3/pm.max_spare_servers = 10/g' /usr/local/etc/php-fpm.d/www.conf \
    && echo "pm.max_requests = 500" >> /usr/local/etc/php/conf.d/magento.ini

#Install composer

RUN curl --insecure https://getcomposer.org/download/1.10.16/composer.phar -o /usr/bin/composer && chmod +x /usr/bin/composer

#Install composer plugin for parallel installs

RUN composer global require hirak/prestissimo

#Add curl cacert.pem

COPY  ./conf/cacert.pem /usr/local/share/ca-certificates/cacert.pem 
RUN composer config --global cafile "/usr/local/share/ca-certificates/cacert.pem"

#Install MailHog

#COPY --from=edspc/mhsendmail:alpine /go/bin/mhsendmail /usr/bin/mhsendmail
RUN apt-get update &&\
    apt-get install --no-install-recommends --assume-yes --quiet ca-certificates curl git &&\
    rm -rf /var/lib/apt/lists/* &&\
    update-ca-certificates -f
RUN curl -Lsf 'https://storage.googleapis.com/golang/go1.8.3.linux-amd64.tar.gz' | tar -C '/usr/local' -xvzf -
ENV PATH /usr/local/go/bin:$PATH
RUN go get github.com/mailhog/mhsendmail
RUN cp /root/go/bin/mhsendmail /usr/bin/mhsendmail
RUN echo 'sendmail_path = /usr/bin/mhsendmail --smtp-addr mailhog:1025' >> /usr/local/etc/php/conf.d/magento.ini

#Configure permissions
RUN usermod -u 1000 www-data && groupmod -g 1000 www-data && \
    chsh -s /bin/bash www-data && chown -R www-data:www-data /var/www/html

WORKDIR /var/www/html
USER www-data


